package com.pgc.dragrecyclerviewdemo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.google.android.flexbox.FlexboxLayoutManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Button view1, view2, view3, view4, view5;
    private List<String> titles1;
    private int viewX1, viewY1, viewWidth1, viewHeight1, viewX2, viewY2, viewWidth2, viewHeight2, viewX3, viewY3, viewWidth3, viewHeight3, viewX4, viewY4, viewWidth4, viewHeight4, viewX5, viewY5, viewWidth5, viewHeight5;
    private int moveType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        view1 = findViewById(R.id.view_1);
        view2 = findViewById(R.id.view_2);
        view3 = findViewById(R.id.view_3);
        view4 = findViewById(R.id.view_4);
        view5 = findViewById(R.id.view_5);
        DragRecyclerView recyclerView = findViewById(R.id.recycler_view);
        initTitles();
        TitleAdapter titleAdapter = new TitleAdapter(R.layout.adapter_title_item, titles1);
        FlexboxLayoutManager flexboxLayoutManager = new FlexboxLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(flexboxLayoutManager);
        recyclerView.setAdapter(titleAdapter);

        recyclerView.setOnItemMoveListener(new DragRecyclerView.OnItemMoveListener() {
            @Override
            public void onMove(int x, int y) {
                if (x > viewX1 && x < viewX1 + viewWidth1 && y > viewY1 && y < viewY1 + viewHeight1) {
                    moveType = 1;
                } else if (x > viewX2 && x < viewX2 + viewWidth2 && y > viewY2 && y < viewY2 + viewHeight2) {
                    moveType = 2;
                } else if (x > viewX3 && x < viewX3 + viewWidth3 && y > viewY3 && y < viewY3 + viewHeight3) {
                    moveType = 3;
                } else if (x > viewX4 && x < viewX4 + viewWidth4 && y > viewY4 && y < viewY4 + viewHeight4) {
                    moveType = 4;
                } else if (x > viewX5 && x < viewX5 + viewWidth5 && y > viewY5 && y < viewY5 + viewHeight5) {
                    moveType = 5;
                } else {
                    moveType = -1;
                }
                if (moveType != -1)
                    Toast.makeText(getApplicationContext(), "移动到VIEW" + moveType, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onUp(int position) {
                Log.d("哦豁",moveType+"----"+position);
            }

            @Override
            public void onItemClick(int position) {
                Log.d("进入","点击了="+position);
            }
        });

        view1.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            int[] location = new int[2];
            view1.getLocationOnScreen(location);
            viewX1 = location[0];
            viewY1 = location[1];
            viewWidth1 = view1.getWidth();
            viewHeight1 = view1.getHeight();
        });

        view2.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            int[] location = new int[2];
            view2.getLocationOnScreen(location);
            viewX2 = location[0];
            viewY2 = location[1];
            viewWidth2 = view2.getWidth();
            viewHeight2 = view2.getHeight();
        });
        view3.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            int[] location = new int[2];
            view3.getLocationOnScreen(location);
            viewX3 = location[0];
            viewY3 = location[1];
            viewWidth3 = view3.getWidth();
            viewHeight3 = view3.getHeight();
        });
        view4.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            int[] location = new int[2];
            view4.getLocationOnScreen(location);
            viewX4 = location[0];
            viewY4 = location[1];
            viewWidth4 = view4.getWidth();
            viewHeight4 = view4.getHeight();
        });
        view5.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            int[] location = new int[2];
            view5.getLocationOnScreen(location);
            viewX5 = location[0];
            viewY5 = location[1];
            viewWidth5 = view5.getWidth();
            viewHeight5 = view5.getHeight();
        });
    }

    private void initTitles() {
        titles1 = new ArrayList<>();
        for (int i = 1; i <= 13; i++) {
            if (i % 3 == 0) {
                titles1.add("我zzZZZZZZZZZZZ是" + i);
            } else if (i % 3 == 1) {
                titles1.add("我是" + i);
            } else {
                titles1.add("我是QQQ" + i);
            }
        }
        Collections.shuffle(titles1);
    }


    private class TitleAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

        public TitleAdapter(int layoutResId, List<String> data) {
            super(layoutResId, data);
        }

        @SuppressLint("WrongConstant")
        @Override
        protected void convert(@NonNull BaseViewHolder baseViewHolder, String s) {
            baseViewHolder.setText(R.id.title_item_tv, s);
        }
    }
}